package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.model.ISessionRepository;
import ru.t1.ytarasov.tm.api.service.model.ISessionService;
import ru.t1.ytarasov.tm.exception.entity.SessionNotFoundException;
import ru.t1.ytarasov.tm.exception.field.IdEmptyException;
import ru.t1.ytarasov.tm.exception.field.UserIdEmptyException;
import ru.t1.ytarasov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Service
public final class SessionService
        extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @Override
    public @Nullable List<Session> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @NotNull
    @Override
    @Transactional
    public Session add(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        repository.add(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Collection<Session> add(@Nullable Collection<Session> models) throws Exception {
        if (models == null || models.isEmpty()) throw new SessionNotFoundException();
        for (@NotNull final Session model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }

    @Nullable
    @Override
    @Transactional
    public Session remove(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Session removeById(@Nullable final String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(id);
        if (session == null) throw new SessionNotFoundException();
        repository.removeById(id);
        return session;
    }

    @NotNull
    @Override
    @Transactional
    public Session update(@Nullable Session model) throws Exception {
        if (model == null) throw new SessionNotFoundException();
        model.setUpdated(new Date());
        repository.update(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public  List<Session> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public Session findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Override
    public @Nullable Session removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new SessionNotFoundException();
        repository.removeById(id, userId);
        return session;
    }

    @NotNull
    @Override
    @Transactional
    public Session add(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new SessionNotFoundException();
        repository.add(userId, model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Session remove(@Nullable String userId, @Nullable Session model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new SessionNotFoundException();
        @Nullable final Session session = findOneById(userId, model.getId());
        if (session == null) throw new SessionNotFoundException();
        return remove(session);
    }

}
