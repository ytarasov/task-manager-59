package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.ytarasov.tm.api.repository.model.IProjectRepository;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.enumerated.Sort;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Service
public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Comparator comparator) throws Exception {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final Sort sort) throws Exception {
        if (sort == null) return findAll();
        else return findAll(sort.getComparator());
    }

    @Nullable
    @Override
    public List<Project> findAll(
            @Nullable final String userId, @Nullable final Comparator comparator
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId, @Nullable final Sort sort) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        else return findAll(userId, sort.getComparator());
    }

    @NotNull
    @Override
    @Transactional
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return add(userId, new Project(name, description));
    }

    @NotNull
    @Override
    @Transactional
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        update(project);
        return project;
    }
    @NotNull
    @Override
    @Transactional
    public Project changeStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        update(project);
        return project;
    }

    @Nullable
    @Override
    public List<Project> findAll() throws Exception {
        return repository.findAll();
    }

    @Override
    public Long getSize() throws Exception {
        return repository.getSize();
    }

    @NotNull
    @Override
    @Transactional
    public Project add(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        repository.add(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Collection<Project> add(@Nullable Collection<Project> models) throws Exception {
        if (models == null || models.isEmpty()) throw new ProjectNotFoundException();
        for (@NotNull final Project model : models) add(model);
        return models;
    }

    @Override
    public boolean existsById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(id);
    }

    @Override
    @Transactional
    public void clear() throws Exception {
        repository.clear();
    }

    @Nullable
    @Override
    @Transactional
    public Project remove(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        repository.remove(model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Project removeById(@Nullable String id) throws Exception {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(id);
        if (project == null) throw new ProjectNotFoundException();
        repository.removeById(id);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project update(@Nullable Project model) throws Exception {
        if (model == null) throw new ProjectNotFoundException();
        model.setUpdated(new Date());
        repository.update(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.clear(userId);
    }

    @Override
    public boolean existsById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.existsById(userId, id);
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.findAll(userId);
    }

    @Nullable
    @Override
    public Project findOneById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findOneById(userId, id);
    }

    @Override
    public Long getSize(@Nullable String userId) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return repository.getSize(userId);
    }

    @Nullable
    @Override
    @Transactional
    public Project removeById(@Nullable String userId, @Nullable String id) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Project project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        repository.removeById(id, userId);
        return project;
    }

    @NotNull
    @Override
    @Transactional
    public Project add(@Nullable String userId, @Nullable Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        repository.add(userId, model);
        return model;
    }

    @Nullable
    @Override
    @Transactional
    public Project remove(@Nullable String userId, @Nullable Project model) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ProjectNotFoundException();
        @Nullable final Project project = findOneById(userId, model.getId());
        if (project == null) throw new ProjectNotFoundException();
        return remove(project);
    }

}
