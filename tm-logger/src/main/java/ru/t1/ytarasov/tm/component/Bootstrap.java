package ru.t1.ytarasov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.ytarasov.tm.api.service.IReceiverService;
import ru.t1.ytarasov.tm.listener.EntityListener;

@Component
public class Bootstrap {

    @NotNull
    @Autowired
    private IReceiverService receiverService;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void init() {
        receiverService.receive(entityListener);
    }
}
