package ru.t1.ytarasov.tm.enumerated;

import org.jetbrains.annotations.NotNull;

public enum Role {

    USUAL("USUAL"),
    ADMIN("ADMIN");

    private @NotNull
    final String displayName;

    Role(@NotNull String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    public static Role toRole(final String value) {
        if (value == null || value.isEmpty()) return null;
        for (final Role role : values()) {
            if (role.getDisplayName().equals(value)) return role;
        }
        return null;
    }

}
